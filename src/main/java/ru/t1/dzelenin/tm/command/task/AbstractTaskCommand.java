package ru.t1.dzelenin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.dzelenin.tm.api.service.IProjectTaskService;
import ru.t1.dzelenin.tm.api.service.ITaskService;
import ru.t1.dzelenin.tm.command.AbstractCommand;
import ru.t1.dzelenin.tm.enumerated.Role;

public abstract class AbstractTaskCommand extends AbstractCommand {

    protected ITaskService getTaskService() {
        return serviceLocator.getTaskService();
    }

    protected IProjectTaskService getProjectTaskService() {
        return serviceLocator.getProjectTaskService();
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}

