package ru.t1.dzelenin.tm.exception;

import org.jetbrains.annotations.NotNull;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public abstract class AbstractException extends RuntimeException {

    public AbstractException(@NotNull final String message) {
        super(message);
    }

    public AbstractException(@NotNull final String message, @NotNull final Throwable cause) {
        super(message, cause);
    }

    public AbstractException(@NotNull final Throwable cause) {
        super(cause);
    }

    public AbstractException(@NotNull final String message,
                             @NotNull final Throwable cause,
                             @NotNull final boolean enableSuppression,
                             @NotNull final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
